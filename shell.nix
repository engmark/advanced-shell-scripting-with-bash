let
  pkgs = import ./nixpkgs.nix;
  content = pkgs.callPackage ./derivation.nix { inherit pkgs; };
in
pkgs.mkShell {
  packages = [
    content
    pkgs.cacert
    pkgs.check-jsonschema
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nixfmt-rfc-style
    pkgs.pre-commit
    pkgs.nodePackages.prettier
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.statix
    pkgs.yarn
    pkgs.yq-go
  ];
  env = {
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };
}
